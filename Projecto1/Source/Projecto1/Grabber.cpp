 // Fill out your copyright notice in the Description page of Project Settings.

#include "Grabber.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "Engine/World.h"
#include "GameFramework/PlayerController.h"
#include "Components/InputComponent.h"

#define OUT

// Sets default values for this component's properties
UGrabber::UGrabber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	// ...
}


// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();
	Owner = GetOwner();
	Handle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
	FindAndBindInput();
}

// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (Handle && Handle->GrabbedComponent)
		Handle->SetTargetLocation(GrabEnd());
}

void UGrabber::Grab()
{
	if (!Handle) { return; }

	auto Hit = GetHitResult();
	auto HitComp = Hit.GetComponent();
	auto HitActor = Hit.GetActor();

	if (HitActor)
		Handle->GrabComponent(HitComp, NAME_None, HitActor->GetActorLocation(), true);
}

void UGrabber::Release()
{
		if (Handle && Handle->GrabbedComponent)
			Handle->ReleaseComponent();
}

//Find and Bind input
void UGrabber::FindAndBindInput()
{
	if (Owner && Owner->InputComponent)
	{
		Owner->InputComponent->BindAction("Grab", IE_Pressed, this, &UGrabber::Grab);
		Owner->InputComponent->BindAction("Grab", IE_Released, this, &UGrabber::Release);
	}
	else
		UE_LOG(LogTemp, Error, TEXT("InputComponent not found."))
}

//Get HitResult by Line Trace
const FHitResult UGrabber::GetHitResult()
{	
	FCollisionQueryParams TraceParam(FName(TEXT("")), false, Owner);
	FHitResult Hit;

	/// Crear LineTrace

	GetWorld()->LineTraceSingleByObjectType(
		OUT Hit,
		GrabStart(),
		GrabEnd(),
		FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody),
		TraceParam
	);
		return Hit;
}

FVector UGrabber::GrabStart()
{
	return Owner->GetActorLocation();
}

FVector UGrabber::GrabEnd()
{
	FVector OwnerPosition;
	FRotator OwnerRotation;
	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(OUT OwnerPosition, OUT OwnerRotation);

	return OwnerPosition + OwnerRotation.Vector() * Reach;
}
