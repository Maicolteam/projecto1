// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "OpenDoorComp.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FEventDoor);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PROJECTO1_API UOpenDoorComp : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UOpenDoorComp();

	UPROPERTY(BlueprintAssignable)
	FEventDoor OnOpenDoor;
	
	UPROPERTY(BlueprintAssignable)
	FEventDoor OnCloseDoor;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:

	UPROPERTY(EditAnywhere)
	class ATriggerVolume* AsociatedTrigger = nullptr;

	class AActor* Owner = nullptr;
	
	UPROPERTY(EditAnywhere)
	float MassToOpenDoor = 50.f;

	float MassOnTrigger();
};
