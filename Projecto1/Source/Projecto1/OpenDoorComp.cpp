// Fill out your copyright notice in the Description page of Project Settings.

#include "OpenDoorComp.h"
#include "Engine/World.h"
#include "GameFramework/PlayerController.h"
#include "Engine/TriggerVolume.h"
#include "Components/PrimitiveComponent.h"

#define OUT

// Sets default values for this component's properties
UOpenDoorComp::UOpenDoorComp()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UOpenDoorComp::BeginPlay()
{
	Super::BeginPlay();
	Owner = GetOwner();
}


// Called every frame
void UOpenDoorComp::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (!AsociatedTrigger) { return; }

	if (MassOnTrigger() > MassToOpenDoor)
	{
		OnOpenDoor.Broadcast();
	}

	else
		OnCloseDoor.Broadcast();
		
}

float UOpenDoorComp::MassOnTrigger()
{
	float MassOnTrigger = 0.0f;
	TArray<AActor*> ActorsOnTrigger;

	AsociatedTrigger->GetOverlappingActors(OUT ActorsOnTrigger);
	for (const auto* Actor : ActorsOnTrigger)
	{
		MassOnTrigger += Actor->FindComponentByClass<UPrimitiveComponent>()->GetMass();
	}
		return MassOnTrigger;
}