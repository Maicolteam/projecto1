// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Grabber.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PROJECTO1_API UGrabber : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGrabber();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	class AActor* Owner = nullptr;
	float Reach = 100.0f;
	class UPhysicsHandleComponent* Handle = nullptr;

	void Grab();
	
	void Release();
	
	void FindAndBindInput();

	FVector GrabStart();
	
	FVector GrabEnd();	

	const FHitResult GetHitResult();
};
